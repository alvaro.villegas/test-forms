import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  @ViewChild('attachments') attachment: any;

  tmpFiles: File[] = [];
  listOfFiles: any[] = [];

  constructor() {}

  addFile(event: any): any {
    for (let i = 0; i <= event.target.files.length - 1; i++) {
      const selectedFile = event.target.files[i];
      this.tmpFiles.push(selectedFile);
      this.listOfFiles.push(selectedFile.name);
      console.log(this.tmpFiles);
    }
    this.attachment.nativeElement.value = '';
  }

  delectedFile(index): any {
    this.listOfFiles.splice(index, 1);
    this.tmpFiles.splice(index, 1);
    console.log(this.tmpFiles);

  }

  ngOnInit(): void {}
}
